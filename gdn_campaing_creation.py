# START: patch introduced during zip construction to avoid versioning conflicts
import sys

existing_mods = sys.modules.copy()
mods_to_clean = [
    "making_science"
]

for mod in existing_mods:
    if mod in mods_to_clean:
        del sys.modules[mod]
    for mod_prefix in mods_to_clean:
        if mod.startswith(mod_prefix + "."):
            del sys.modules[mod]
# END: patch introduced during zip construction to avoid versioning conflicts

import json
from datetime import timedelta
from datetime import datetime
from google.cloud import bigquery
from google.cloud import storage
from google.oauth2 import service_account
from google.ads.googleads.client import GoogleAdsClient

from airflow import DAG
from airflow.hooks.base_hook import BaseHook
from airflow.operators.python_operator import PythonOperator
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.models import Variable

from making_science.gdn_campaign_creation.utils.start_program import start_program

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'gonzalo_martin',
    'depends_on_past': False,
    'start_date': datetime(2022, 2, 8, 10, 00),
    'email': ['gonzalo.martin@makingscience.com'],
    'email_on_failure': ['gonzalo.martin@makingscience.com'],
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(),
    'provide_context': True,
    'catchup': False
}

dag = DAG(
    'gdn_campaign_creation',
    description='Program to create campaigns in google_ads based on feed stored on BQ',
    schedule_interval='0 */1 * * *',
    default_args=default_args,
)

gdn_start = DummyOperator(
    task_id='gdn_start',
    trigger_rule='all_success',
    dag=dag
)

gdn_end = DummyOperator(
    task_id='gdn_end',
    trigger_rule='none_failed',
    dag=dag
)


# BranchPyhonOperator call to this function to decide if the programme needs to work or not
def get_and_decide(**kwargs):

    connection = BaseHook.get_connection('gdn_storage_and_bq')
    service_account_info = connection.extra_dejson['extra__google_cloud_platform__keyfile_dict']
    service_account_info = json.loads(service_account_info)
    
    credentials = service_account.Credentials.from_service_account_info(service_account_info)
    bq_client = bigquery.Client(project='ms-guadiana', credentials=credentials)

    # Perform basic query to know if there are data to work
    gdn_data_query = """
        SELECT account_id FROM `ms-guadiana.gdn_campaign_creation.request_data` LIMIT 10 
        """
    query_job = bq_client.query(gdn_data_query)
    rows = query_job.result()

    # If no data to work returns and stop program
    if rows.total_rows == 0:
        return "there_is_no_data"

    try:
        # Perform query to create table with the data to work with it.
        kw_gsheet_sql = """
        CREATE OR REPLACE TABLE `ms-guadiana.gdn_campaign_creation.request_data_temp`
            AS
        SELECT * FROM `ms-guadiana.gdn_campaign_creation.request_data` LIMIT 50 
        """
        query_job = bq_client.query(kw_gsheet_sql)
        rows = query_job.result()

    except Exception as e:
        print(e)

    return "there_is_data"


def start(**kwargs):
    connection = BaseHook.get_connection('gdn_storage_and_bq')
    service_account_info = connection.extra_dejson['extra__google_cloud_platform__keyfile_dict']
    service_account_info = json.loads(service_account_info)
    bq_credentials = service_account.Credentials.from_service_account_info(service_account_info)
    bq_client = bigquery.Client(project='ms-guadiana', credentials=bq_credentials)
    
    # use bq_credentials beacuse is the same service account to BQ and GCS
    storage_client = storage.Client(project='ms-guadiana', credentials=bq_credentials)
    
    google_ads_credentials = json.loads(Variable.get('gdn_campaign_creation_gads_credentials'))
    unsplash_token = json.loads(Variable.get('gdn_campaign_creation_unsplash_token'))
    bucket_name = "gdn_campaign_creation"
    start_program(bq_client, storage_client, bq_credentials, unsplash_token, google_ads_credentials, bucket_name)


to_skip_work = DummyOperator(dag=dag, task_id='there_is_no_data')
to_do_work = DummyOperator(dag=dag, task_id='there_is_data')
gdn_work = PythonOperator(task_id='gdn_work', dag=dag, python_callable=start)

branch_task = BranchPythonOperator(task_id='data_branch_conditional',
                python_callable=get_and_decide,
                provide_context = True,
                dag=dag)
branch_task >> to_do_work >> gdn_work >> gdn_end
branch_task >> to_skip_work >> gdn_end

gdn_start >> branch_task 
