import pandas as pd
import json

from io import StringIO
from making_science.gdn_campaign_creation.utils.unsplash import unsplash_download
from google.cloud import storage
from google.cloud import bigquery
from google.oauth2 import service_account

from airflow.models import Variable

def search_images(bq_client, storage_client, category, subcategory, keyword, images_per_page, number_of_pages, bucket_name, access_token):
    marketing_images = []
    square_images = []
    regular_images = []
    
    query_string = """
                SELECT
                    *
                FROM `ms-guadiana.gdn_campaign_creation.downloaded_images_history` as a
                WHERE
                    a.keyword = '{}'
                """.format(keyword, category, subcategory)
    response =  bq_client.query(query_string)
    
    for result in response:
        if result._size_ == "marketing_img":
            marketing_images.append(result.url)
        if result._size_ == "square_img":
            square_images.append(result.url)
        if result._size_ == "regular":
            regular_images.append(result.url)
    if marketing_images and square_images and regular_images:
        print("Images found in the database in the first search -------", keyword)
        return marketing_images, square_images, regular_images
    else:
        unsplash_download(storage_client, bucket_name, category, subcategory, keyword, images_per_page, number_of_pages, access_token)
    
    response =  bq_client.query(query_string)
    for result in response:
        if result._size_ == "marketing_img":
            marketing_images.append(result.url)
        if result._size_ == "square_img":
            square_images.append(result.url)
        if result._size_ == "regular":
            regular_images.append(result.url)
    
    return marketing_images, square_images, regular_images