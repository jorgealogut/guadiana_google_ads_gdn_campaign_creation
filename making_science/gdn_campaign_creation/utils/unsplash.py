import requests
from google.cloud import storage
from PIL import Image
import io
import re
from datetime import datetime       
import uuid
import pandas as pd
from google.oauth2 import service_account
import json
from airflow.models import Variable

def save_image(image, name, bucket_name, category, subcategory, keyword, results, bucket, data_ID,rows_to_append):
    # open and get image data
    img1 = Image.open(io.BytesIO(image.content))

    # upload marketing image to google cloud
    storage_path = category+"/"+subcategory+"/"+keyword+"/"+name+"/"+results["id"]+".png"
    blob = bucket.blob(storage_path)
    buf = io.BytesIO()
    img1.save(buf, 'png')
    img1 = buf.getvalue()
    blob.upload_from_string(img1, 'image/png')
    storage_path = re.sub(' ', '%20', storage_path)
    now = datetime.now()
    creation_date = now.strftime('%Y-%m-%d_%H:%M:%S')
    
    #return the uploads images info to save it
    list_of_elem = { 
        'bucket_name': bucket_name, 
        'category': category, 
        'subcategory': subcategory, 
        'keyword': keyword,
        '_size_': name,
        'url': "https://storage.googleapis.com/"+bucket_name+"/"+storage_path, 
        'creation_date': creation_date, 
        'criteria_ID': data_ID}

    
    rows_to_append.append(list_of_elem)
    return rows_to_append


def unsplash_download(storage_client, bucket_name, category, subcategory, keyword, images_per_page, number_of_pages,access_token):

    # service_account_info = json.loads(Variable.get('gdn_campaign_creation_service_account'))
    # credentials = service_account.Credentials.from_service_account_info(service_account_info)

    # client = storage.Client(project='ms-guadiana', credentials=credentials)
    bucket = storage_client.get_bucket(bucket_name)

    # Generete an Unique_Id for this criteria
    data_ID = str(uuid.uuid1())

    # Seting search criteria 

    unsplash_url = "https://api.unsplash.com/search/photos"

    query = "?query=" + keyword

    quantity = "&per_page="+ str(images_per_page) +"&page="+ str(number_of_pages)

    client_id = "&client_id=" + access_token

    search_url = unsplash_url + query + quantity + client_id


    # Search with required parameters 

    response = requests.get(search_url)
    img_urls  = response.json()

    # For each resulting url save that image 
    rows_to_append = []
    index = 0
    for results in img_urls["results"]:

        # go to resulting url and "download" data
        if index == 0:
            response_marketing_img = requests.get(results["urls"]['raw'] + "&w=600&h=314&fit=crop") 
            response_square_img = requests.get(results["urls"]['raw'] + "&w=300&h=300&fit=crop") 
            rows_to_append = save_image(response_marketing_img, 'marketing_img', bucket_name, category, subcategory, keyword, results, bucket, data_ID, rows_to_append)
            rows_to_append = save_image(response_square_img, 'square_img', bucket_name, category, subcategory, keyword, results, bucket, data_ID, rows_to_append)
          
        else:
            img = requests.get(results["urls"]['raw'] + "&w=1200&h=628&fit=crop")
            rows_to_append = save_image(img, 'regular', bucket_name, category, subcategory, keyword, results, bucket, data_ID, rows_to_append)
           
        index += 1
        print(results["id"] + ".png", "SUBIDA")

    #finally upload that info to downloaded_images_history table
    if rows_to_append:
        rec_row_df = pd.DataFrame(rows_to_append, columns=['bucket_name','category','subcategory','keyword','_size_','url','creation_date','criteria_ID']) 
        rec_row_df.to_gbq('gdn_campaign_creation.downloaded_images_history', if_exists='append', project_id='ms-guadiana', credentials=credentials)
        print("Images added to downloded_images_history")