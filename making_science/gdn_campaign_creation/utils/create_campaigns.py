import uuid
import google.ads.googleads.client
from google.ads.googleads.errors import GoogleAdsException
import pandas as pd

_DATE_FORMAT = "%Y%m%d"

def create_campaign(client, customer_id, iloc):
    error = ""
    campaign_budget_service = client.get_service("CampaignBudgetService")
    campaign_service = client.get_service("CampaignService")

    # Create a budget, which can be shared by multiple campaigns.
    campaign_budget_operation = client.get_type("CampaignBudgetOperation")

    campaign_budget = campaign_budget_operation.create
    campaign_budget.name =  "%s %s" % (iloc.campaign_name, uuid.uuid4())             # Google example ad a uuid4:  "%s %s" % (budget, uuid.uuid4())
    campaign_budget.delivery_method = client.get_type(
        "BudgetDeliveryMethodEnum"
    ).BudgetDeliveryMethod.STANDARD
    campaign_budget.amount_micros = iloc.campaign_budget * 10000
    campaign_budget.explicitly_shared = False

    # Add budget.
    try:
        campaign_budget_response = campaign_budget_service.mutate_campaign_budgets(
            customer_id=customer_id, operations=[campaign_budget_operation]
        )
    except google.ads.googleads.errors.GoogleAdsException as ex:
        print(
            'Request-Add-Budget with ID "%s" failed with status "%s" and includes the '
            "following errors:" % (ex.request_id, ex.error.code().name)
        )
        for err in ex.failure.errors:
            print('\tError with message "%s".' % err.message)
            # if error.location:
            #     for field_path_element in error.location.field_path_elements:
            #         print("\t\tOn field: %s" % field_path_element.field_name)
            error = err.message 
        created_campaign_id = -1
        return created_campaign_id, error

    # Create campaign.
    campaign_operation = client.get_type("CampaignOperation")
    campaign = campaign_operation.create
    campaign.name = iloc.campaign_name                                         # Google example ad a uuid4: "%s %s" % (name, uuid.uuid4())
    campaign.geo_target_type_setting.positive_geo_target_type = 7
    campaign.advertising_channel_type = client.get_type(
        "AdvertisingChannelTypeEnum"
    ).AdvertisingChannelType.DISPLAY


    # Recommendation: Set the campaign to PAUSED when creating it to prevent
    # the ads from immediately serving. Set to ENABLED once you've added
    # targeting and the ads are ready to serve.
    campaign.status = client.get_type("CampaignStatusEnum").CampaignStatus.ENABLED

    # Set the bidding strategy and budget.
    if iloc.BiddingStrategyType == "MANUAL_CPC":
        campaign.manual_cpc.enhanced_cpc_enabled = iloc.enhanced_cpc_enabled
    elif iloc.BiddingStrategyType == "MAXIMIZE_CONVERSIONS":      
        if pd.notnull(iloc.target_cpa):
            campaign.maximize_conversions.target_cpa = int(iloc.target_cpa)
        # else:
        #     campaign.maximize_conversions = campaign.maximize_conversions

    campaign.campaign_budget = campaign_budget_response.results[0].resource_name

    # Set the campaign network options.
    campaign.network_settings.target_google_search = False
    campaign.network_settings.target_search_network = False
    campaign.network_settings.target_content_network = True
    campaign.network_settings.target_partner_search_network = False
    # [END add_campaigns]

    # # Optional: Set the start date.
    # start_time = datetime.date.today() + datetime.timedelta(days=1)
    # campaign.start_date = datetime.date.strftime(start_time, _DATE_FORMAT)

    # # Optional: Set the end date.
    # end_time = start_time + datetime.timedelta(weeks=4)
    # campaign.end_date = datetime.date.strftime(end_time, _DATE_FORMAT)

    # Add the campaign.
    try:
        campaign_response = campaign_service.mutate_campaigns(
            customer_id=customer_id, operations=[campaign_operation]
        )
        created_campaign_id = campaign_response.results[0].resource_name.split('/')
        created_campaign_id = created_campaign_id[3]
        print(
            "Created campaign with resource ID:"
            f"'{created_campaign_id}'."
        )
    except google.ads.googleads.errors.GoogleAdsException as ex:
        print(
            'Request-Add-Capaign with ID "%s" failed with status "%s" and includes the '
            "following errors:" % (ex.request_id, ex.error.code().name)
        )
        for err in ex.failure.errors:
            print('\tError with message "%s".' % err.message)
            # if er.location:
            #     for field_path_element in error.location.field_path_elements:
            #         print("\t\tOn field: %s" % field_path_element.field_name)
            error = err.message
        created_campaign_id = -1


    return created_campaign_id, error