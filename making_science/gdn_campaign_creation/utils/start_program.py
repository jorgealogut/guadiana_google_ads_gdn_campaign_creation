import pandas as pd
from io import StringIO
import json
from google.cloud import bigquery

from making_science.gdn_campaign_creation.utils.get_campaigns import get_campaigns
from making_science.gdn_campaign_creation.utils.get_ad_groups import get_ad_groups
from making_science.gdn_campaign_creation.utils.get_ads import get_ads
from making_science.gdn_campaign_creation.utils.create_campaigns import  create_campaign
from making_science.gdn_campaign_creation.utils.create_add_groups import create_add_group
from making_science.gdn_campaign_creation.utils.create_asset import create_ads
from making_science.gdn_campaign_creation.utils.search_images import search_images
from making_science.gdn_campaign_creation.utils.set_campaign_criteria import set_campaign_criteria

from airflow.models import Variable

from google.ads.googleads.client import GoogleAdsClient
from google.oauth2 import service_account
from google.cloud import storage
import datetime


_DEFAULT_PAGE_SIZE = 1000

def start_program(bq_client, storage_client, bq_credentials, access_token, google_ads_credentials, bucket_name):

    images_per_page = 10
    number_of_pages= 1

    today_date = datetime.datetime.now()
    today_date = today_date.strftime("%Y%m%d")

    #set client
    google_ads_client = GoogleAdsClient.load_from_dict(google_ads_credentials)

    #set custom page size, google default is 1000
    page_size = _DEFAULT_PAGE_SIZE

    gdn_request_data_query = """
        SELECT * FROM `ms-guadiana.gdn_campaign_creation.request_data_temp`
        """
    query_job = bq_client.query(gdn_request_data_query)
    result = query_job.result()
    

    # set as false to only make one search in the first loop
    campaigns = False
    ads = False

    #for each row in csv info, search and create if is necesary
    registry_done = []
    registry_failed = []
    for iloc in result:
       
        # Initialize variables each loop 
        keyword = iloc.keyword
        category = iloc.category
        subcategory = iloc.subcategory
        campaign_id = 0
        campaign_name = ""
        ad_group_id = 0
        ad_group_name = ""
        ads_exist = 0
        customer_id = iloc.account_id.replace("-", "")

        special_characters = "!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"
        if any(c in special_characters for c in keyword):
            registry_failed.append(["Keyword can't use special characters like: " + special_characters, iloc.account_id, iloc.account_name, iloc.campaign_name, iloc.campaign_budget, iloc.location_id, iloc.conversion_action, iloc.adgroup_name, iloc.final_url, iloc.headline_1, iloc.headline_2, iloc.headline_3, iloc.headline_4, iloc.headline_5, iloc.long_headline, iloc.desc_line_1, iloc.desc_line_2, iloc.desc_line_3, iloc.desc_line_4, iloc.business_name, iloc.category, iloc.subcategory, iloc.keyword, iloc.call_to_action])
            continue
        
        # This avoids searching for campaigns and ads for each loop, just search for the first loop.
        if not campaigns:
            campaigns = get_campaigns(google_ads_client, customer_id)
            ads = get_ads(google_ads_client, customer_id, page_size)

        # First of all find if each ads exists
        for row in ads:
            # Find if that ads exists
            if iloc.final_url == row.ad.final_urls[0]:
                ads_exist = 1
                print(row.ad.final_urls[0], "-------------------Ads EXISTS")
                break

        # If that ads exists stop this iteration and continue whit a new row
        if ads_exist == 1:
            registry_failed.append(["Ads EXISTS", iloc.account_id, iloc.account_name, iloc.campaign_name, iloc.campaign_budget, iloc.location_id, iloc.conversion_action, iloc.adgroup_name, iloc.final_url, iloc.headline_1, iloc.headline_2, iloc.headline_3, iloc.headline_4, iloc.headline_5, iloc.long_headline, iloc.desc_line_1, iloc.desc_line_2, iloc.desc_line_3, iloc.desc_line_4, iloc.business_name, iloc.category, iloc.subcategory, iloc.keyword, iloc.call_to_action])
            continue

        # Find if that campaign exists  
        if campaigns:
            for row in campaigns:
                #If exists, take the data to use before when create or search ad_group 
                if str(iloc.campaign_name) == str(row.campaign.name):
                    campaign_id = row.campaign.id
                    campaign_name = iloc.campaign_name
                    campaign_budget = iloc.campaign_budget
                    if row.campaign.bidding_strategy_type != iloc.BiddingStrategyType:
                        print(iloc.campaign_name, "-------------------Campaign EXISTS but need to change bidding_strategy_type ")
                        set_campaign_criteria(google_ads_client, customer_id, campaign_id, iloc)
                    else:
                        print(iloc.campaign_name, "-------------------Campaign EXISTS")
                    break
        

        #If campaign doesn't exists create new campaign
        if campaign_id == 0 or not campaigns:
            campaign_id, error = create_campaign(google_ads_client, customer_id, iloc)
            
        # case that create_campaign has an error
        if campaign_id == -1:
            registry_failed.append([error, iloc.account_id, iloc.account_name, iloc.campaign_name, iloc.campaign_budget, iloc.location_id, iloc.conversion_action, iloc.adgroup_name, iloc.final_url, iloc.headline_1, iloc.headline_2, iloc.headline_3, iloc.headline_4, iloc.headline_5, iloc.long_headline, iloc.desc_line_1, iloc.desc_line_2, iloc.desc_line_3, iloc.desc_line_4, iloc.business_name, iloc.category, iloc.subcategory, iloc.keyword, iloc.call_to_action])
            continue
        else:
            set_campaign_criteria(google_ads_client, customer_id, campaign_id, iloc)

        ad_groups = get_ad_groups(google_ads_client, customer_id, page_size, campaign_id)
        
        # Find if that add_group exists
        #If ad_group exists, use its data to find or create ad_group
        if ad_groups:
            for row in ad_groups:
                #If exists take the data
                if iloc.adgroup_name == row.ad_group.name:
                    ad_group_id = row.ad_group.id
                    ad_group_name = iloc.adgroup_name
                    print(iloc.adgroup_name, "-------------------Ad_group EXISTS")
                    break

        #If ad_group doesn't exists create new ad_group
        if ad_group_id == 0:
           ad_group_id = create_add_group(google_ads_client, customer_id, campaign_id, iloc.adgroup_name, iloc.campaign_budget)
        if ad_group_id == -1:
            registry_failed.append(["Ad_gruop_creation FAILED", iloc.account_id, iloc.account_name, iloc.campaign_name, iloc.campaign_budget, iloc.location_id, iloc.conversion_action, iloc.adgroup_name, iloc.final_url, iloc.headline_1, iloc.headline_2, iloc.headline_3, iloc.headline_4, iloc.headline_5, iloc.long_headline, iloc.desc_line_1, iloc.desc_line_2, iloc.desc_line_3, iloc.desc_line_4, iloc.business_name, iloc.category, iloc.subcategory, iloc.keyword, iloc.call_to_action])
            continue

        #If our program have continue to that point is beacause the ads doesn't exist, if ads exists break before at the start.
        ad_group_resource_name = 'customers/%s/adGroups/%s' % (customer_id, ad_group_id)
        headlines_list = [iloc.headline_1, iloc.headline_2, iloc.headline_3, iloc.headline_4, iloc.headline_5]
        description_list = [iloc.desc_line_1, iloc.desc_line_2, iloc.desc_line_3, iloc.desc_line_4]
        marketing_imgs, square_imgs, regular_imgs = search_images(bq_client, storage_client, category, subcategory, keyword, images_per_page, number_of_pages, bucket_name, access_token)
        
        if marketing_imgs and square_imgs:
            ads_id = create_ads(google_ads_client, customer_id, ad_group_resource_name, iloc.final_url, headlines_list, iloc.long_headline, description_list, iloc.business_name, marketing_imgs, square_imgs,regular_imgs)
            if ads_id == -1:
                registry_failed.append(["Ads_creation_failed", iloc.account_id, iloc.account_name, iloc.campaign_name, iloc.campaign_budget, iloc.location_id, iloc.conversion_action, iloc.adgroup_name, iloc.final_url, iloc.headline_1, iloc.headline_2, iloc.headline_3, iloc.headline_4, iloc.headline_5, iloc.long_headline, iloc.desc_line_1, iloc.desc_line_2, iloc.desc_line_3, iloc.desc_line_4, iloc.business_name, iloc.category, iloc.subcategory, iloc.keyword, iloc.call_to_action])
                continue
            else:
                registry_done.append([iloc.account_id, iloc.account_name, iloc.campaign_name, iloc.campaign_budget, iloc.location_id, iloc.conversion_action, iloc.adgroup_name, iloc.final_url, iloc.headline_1, iloc.headline_2, iloc.headline_3, iloc.headline_4, iloc.headline_5, iloc.long_headline, iloc.desc_line_1, iloc.desc_line_2, iloc.desc_line_3, iloc.desc_line_4, iloc.business_name, iloc.category, iloc.subcategory, iloc.keyword, iloc.call_to_action])
        else:
            registry_failed.append(["Ads_creation_failed", iloc.account_id, iloc.account_name, iloc.campaign_name, iloc.campaign_budget, iloc.location_id, iloc.conversion_action, iloc.adgroup_name, iloc.final_url, iloc.headline_1, iloc.headline_2, iloc.headline_3, iloc.headline_4, iloc.headline_5, iloc.long_headline, iloc.desc_line_1, iloc.desc_line_2, iloc.desc_line_3, iloc.desc_line_4, iloc.business_name, iloc.category, iloc.subcategory, iloc.keyword, iloc.call_to_action])

    
    # SAVE REGISTRIES FAILED AND CREATED
    rec_df = pd.DataFrame(registry_failed, columns=['error', 'account_id','account_name','campaign_name','campaign_budget','location_id','conversion_action','adgroup_name','final_url','headline_1','headline_2','headline_3','headline_4','headline_5','long_headline','desc_line_1','desc_line_2','desc_line_3','desc_line_4','business_name','category','subcategory','keyword','call_to_action']) 
    rec_df.to_gbq('gdn_campaign_creation.failed_ads_registry_' + today_date, if_exists='append', project_id='ms-guadiana', credentials=bq_credentials)
    
    if registry_done:
        rec_df = pd.DataFrame(registry_done, columns=['account_id','account_name','campaign_name','campaign_budget','location_id','conversion_action','adgroup_name','final_url','headline_1','headline_2','headline_3','headline_4','headline_5','long_headline','desc_line_1','desc_line_2','desc_line_3','desc_line_4','business_name','category','subcategory','keyword','call_to_action']) 
        rec_df.to_gbq('gdn_campaign_creation.created_ads_registry_' + today_date, if_exists='append', project_id='ms-guadiana', credentials=bq_credentials)

    # UPDATE REQUEST DATA TABLE WITHOUT THE DATA THAT THE PROGRAM USE THIS TIME TO WORK WITH IT
    query = """
        CREATE OR REPLACE TABLE `ms-guadiana.gdn_campaign_creation.request_data`
            AS
        SELECT * FROM `ms-guadiana.gdn_campaign_creation.request_data`
        EXCEPT DISTINCT SELECT * FROM `ms-guadiana.gdn_campaign_creation.request_data_temp`
        """
    
    query_job = bq_client.query(query)
    response = query_job.result()


