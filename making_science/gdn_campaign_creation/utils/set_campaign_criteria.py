def set_language(client, customer_id, campaign_id, iloc):
    campaign_service = client.get_service("CampaignService")
    campaign_criteria_operation = client.get_type("CampaignCriterionOperation")
    campaign_criteria = campaign_criteria_operation.create
    campaign_criteria.campaign = campaign_service.campaign_path(customer_id, campaign_id)

    language_constant_service = client.get_service('LanguageConstantService')
    campaign_criteria.language.language_constant = language_constant_service.language_constant_path(
        iloc.language
        ) #ej: 1003
    return campaign_criteria_operation

def set_location(client, customer_id, campaign_id, iloc):
    campaign_service = client.get_service("CampaignService")
    campaign_criteria_operation = client.get_type("CampaignCriterionOperation")
    campaign_criteria = campaign_criteria_operation.create
    campaign_criteria.campaign = campaign_service.campaign_path(customer_id, campaign_id)

    geo_target_constant_service = client.get_service( "GeoTargetConstantService")
    campaign_criteria.location.geo_target_constant = geo_target_constant_service.geo_target_constant_path(
        iloc.location_id
    )
    return campaign_criteria_operation

def set_device_bid_modifier(client, customer_id, campaign_id, bid_modifier, device):
    
    campaign_service = client.get_service("CampaignService")
    campaign_criteria_operation = client.get_type("CampaignCriterionOperation")
    campaign_criteria = campaign_criteria_operation.create
    
    # Get campaign.
    campaign_criteria.campaign = campaign_service.campaign_path(customer_id, campaign_id)
    
    # Set bid modifier.
    campaign_criteria.bid_modifier = bid_modifier

    # Set device
    campaign_criteria.device.type_ = device

    return campaign_criteria_operation


def set_campaign_criteria(client, customer_id, campaign_id, iloc):

    campaign_criteria_service = client.get_service("CampaignCriterionService")

    # Set the bid modifier for each device
    campaign_criteria_operations = []
    for device in ["DESKTOP", "TABLET", "MOBILE"]:
        if iloc.device == device[0]:
            bid_modifier = 1 # 0
        else:
            bid_modifier = 0
        campaign_criteria_operation =  set_device_bid_modifier(client, customer_id, campaign_id, bid_modifier, device)
        campaign_criteria_operations.append(campaign_criteria_operation)
    
    # Set language
    campaign_criteria_operation = set_language(client, customer_id, campaign_id, iloc)
    campaign_criteria_operations.append(campaign_criteria_operation)
    
    # Set location
    campaign_criteria_operation = set_location(client, customer_id, campaign_id, iloc)
    campaign_criteria_operations.append(campaign_criteria_operation)


    request = client.get_type("MutateCampaignCriteriaRequest")
    request.customer_id = customer_id
    request.operations = campaign_criteria_operations
    request.response_content_type = (
        client.enums.ResponseContentTypeEnum.MUTABLE_RESOURCE
    )

    campaign_bm_response = campaign_criteria_service.mutate_campaign_criteria(
        request=request
    )
    mutable_resource = campaign_bm_response
    print(
        "Created campaign bid modifiers for campaign "
        f"'{campaign_id}'"
    )