import requests
import google.ads.googleads.client
from google.ads.googleads.errors import GoogleAdsException

def create_ads(client, customer_id, ad_group_resource_name, final_url, headlines, long_headline, descriptions, business_name, marketing_img, square_img, regular_imgs):
    # Get the AdGroupAdService client.
    ad_group_ad_service = client.get_service("AdGroupAdService")

    # Upload image assets for the ad.
    marketing_image_resource_name = upload_image_asset(
        client, customer_id, marketing_img[0], "Marketing Image")

    square_marketing_image_resource_name = upload_image_asset(
        client, customer_id, square_img[0], "Square Marketing Image")

    if marketing_image_resource_name == -1 or square_marketing_image_resource_name == -1:
        return -1

    # Create the relevant asset objects for the ad.
    marketing_image = client.get_type("AdImageAsset")
    marketing_image.asset = marketing_image_resource_name
    square_marketing_image = client.get_type("AdImageAsset")
    square_marketing_image.asset = square_marketing_image_resource_name
    
    regular_img_list = []
    index = 0
    for img in regular_imgs:
        name = "regular_"+str(index)
        name =  upload_image_asset(client, customer_id, img, name)
        if name == -1:
            print("EXCEPTION IN MUTATE ASSET continue wtihoutit -------------------------------------------------")
            continue
        regular_img = client.get_type("AdImageAsset")
        regular_img.asset = name
        regular_img_list.append(regular_img)
        index += 1

    headlines_list = []
    for elm in headlines:
        headline = client.get_type("AdTextAsset")
        headline.text = elm[0:30]
        headlines_list.append(headline)

    descriptions_list = []
    for elm in descriptions:
        description = client.get_type("AdTextAsset")
        description.text = elm[0:90]
        descriptions_list.append(description)


    # Create an ad group ad operation and set the ad group ad values.
    ad_group_ad_operation = client.get_type("AdGroupAdOperation")
    ad_group_ad = ad_group_ad_operation.create
    ad_group_ad.ad_group = ad_group_resource_name
    ad_group_ad.ad.final_urls.append(final_url)   #-----------------------------------add final URL

    # Configure the responsive display ad info object.
    responsive_display_ad_info = ad_group_ad.ad.responsive_display_ad
    responsive_display_ad_info.marketing_images.append(marketing_image)
    if len(regular_img_list) > 0:
        for img in regular_img_list:
            try:
                responsive_display_ad_info.marketing_images.append(img)
            except GoogleAdsException as ex:
                print("excepcion al hacer el apend de las regular", ex.error.code().name)
                continue
    responsive_display_ad_info.square_marketing_images.append(square_marketing_image)

    for headline in headlines_list:
        responsive_display_ad_info.headlines.append(headline)

    responsive_display_ad_info.long_headline.text = long_headline[0:90]

    for description in descriptions_list:
        responsive_display_ad_info.descriptions.append(description)

    responsive_display_ad_info.business_name = business_name

    # Optional: Call to action text.
    # Valid texts: https://support.google.com/google-ads/answer/7005917
    responsive_display_ad_info.call_to_action_text = "Learn More"
    # Optional: Set the ad colors.
    responsive_display_ad_info.main_color = "#0000ff"
    responsive_display_ad_info.accent_color = "#ffff00"
    # Optional: Set to false to strictly render the ad using the colors.
    responsive_display_ad_info.allow_flexible_color = False
    # Optional: Set the format setting that the ad will be served in.
    responsive_display_ad_info.format_setting = client.get_type(
        "DisplayAdFormatSettingEnum"
    ).DisplayAdFormatSetting.NON_NATIVE
    
    # Issue a mutate request to add the ad group ad.
    try:
        ad_group_ad_response = ad_group_ad_service.mutate_ad_group_ads(
            customer_id=customer_id, operations=[ad_group_ad_operation]
        )
        print(
            "Created ads ad with resource name "
            f"'{ad_group_ad_response.results[0].resource_name}'."
        )
        asset_id = ad_group_ad_response.results[0].resource_name.split('~')[1]
    except GoogleAdsException as ex:
        asset_id = -1
        print("EXCEPTION 1------------------------------------------>", ex.error.code().name)
    return asset_id
    

def upload_image_asset(client, customer_id, image_url, asset_name):
 
    # Fetch the image data.
    image_data = requests.get(image_url).content

    # Get the AssetService client. Create an asset operation and set the image asset values.
    asset_operation = client.get_type("AssetOperation")
    asset = asset_operation.create
    asset_type_enum = client.get_type("AssetTypeEnum").AssetType
    asset.type_ = asset_type_enum.IMAGE
    image_asset = asset.image_asset
    image_asset.data = image_data
    image_asset.full_size.url = image_url
    # asset.image_asset.data = image_data
    # asset.name = asset_name                   #DO NOT SET, FIX THE ERROR WITH DUPLICATED ASSETS NAMES

    asset_service = client.get_service("AssetService")
    content_type_enum = client.get_type(
        "ResponseContentTypeEnum"
    ).ResponseContentType

    request = client.get_type("MutateAssetsRequest")
    request.customer_id = customer_id
    request.operations = [asset_operation]
    # Setting this parameter tells the API to return the Asset object in the
    # response, allowing us to easily retrieve its ID.
    request.response_content_type = content_type_enum.MUTABLE_RESOURCE
    try:
        # print("INTENTANDO SUBIR ASSET:", image_url)
        mutate_asset_response = asset_service.mutate_assets(request=request)
        image_asset_resource_name = mutate_asset_response.results[0].resource_name
        # print("ASSET SUBIDO OK", image_asset_resource_name)
        return image_asset_resource_name
    except GoogleAdsException as ex:
        print("EXCEPTION IN MUTATE ASSET------------------------------------------>", ex.error.code().name)
        return -1
