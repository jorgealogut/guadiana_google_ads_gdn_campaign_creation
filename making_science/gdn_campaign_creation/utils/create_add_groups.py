import google.ads.googleads.client
from google.ads.googleads.errors import GoogleAdsException

def create_add_group(client, customer_id, campaign_id, adgroup_name, budget):
    ad_group_service = client.get_service("AdGroupService")
    campaign_service = client.get_service("CampaignService")

    # Create ad group.
    ad_group_operation = client.get_type("AdGroupOperation")
    ad_group = ad_group_operation.create
    ad_group.name = str(adgroup_name)         # Google example ad a uuid4: "%s %s" % (adgroup_name, uuid.uuid4())
    ad_group.status = client.get_type("AdGroupStatusEnum").AdGroupStatus.ENABLED
    ad_group.campaign = campaign_service.campaign_path(customer_id, campaign_id)
    ad_group.type_ = client.get_type(
        "AdGroupTypeEnum"
    ).AdGroupType.DISPLAY_STANDARD
    ad_group.cpc_bid_micros = 10000       # The maximum CPC (cost-per-click) bid

    # Add the ad group.
    try:
        ad_group_response = ad_group_service.mutate_ad_groups(
             customer_id=customer_id, operations=[ad_group_operation]
        )
        created_ad_group_id = ad_group_response.results[0].resource_name.split('/')
        created_ad_group_id = created_ad_group_id[3]
        print(
            "Created ad_group with resource ID:"
            f"'{created_ad_group_id}'."
        )
    except google.ads.googleads.errors.GoogleAdsException as ex:
        print(
            'Request with ID "%s" failed with status "%s" and includes the '
            "following errors:" % (ex.request_id, ex.error.code().name)
        )
        for error in ex.failure.errors:
            print('\tError with message "%s".' % error.message)
            if error.location:
                for field_path_element in error.location.field_path_elements:
                    print("\t\tOn field: %s" % field_path_element.field_name)
        created_ad_group_id = -1

    return created_ad_group_id

