import google.ads.googleads.client 

def get_ads(client, customer_id, pages_size):

    # STATUS CAN BE = "ENABLED; REMOVED; PAUSED;" 

    ga_service = client.get_service("GoogleAdsService")
    query = f"""
        SELECT
          ad_group_ad.ad.id,
          ad_group_ad.ad.expanded_text_ad.headline_part1,
          ad_group_ad.ad.expanded_text_ad.headline_part2,
          ad_group_ad.status, ad_group_ad.ad.final_urls,
          ad_group_ad.resource_name
        FROM ad_group_ad    
        WHERE
            ad_group_ad.status = "ENABLED"
        """
    
    ga_search_request = client.get_type("SearchGoogleAdsRequest")
    ga_search_request.customer_id = customer_id
    ga_search_request.query = query
    ga_search_request.page_size = pages_size
    response = ga_service.search(request=ga_search_request)
    response = iter(response)
    ads = []

    while response:
        try:
            current_row = next(response)
            ads.append(current_row.ad_group_ad)
        except StopIteration:
            break
    
    return ads
