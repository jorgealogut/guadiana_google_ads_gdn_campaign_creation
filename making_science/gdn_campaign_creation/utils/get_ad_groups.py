import google.ads.googleads.client 

def get_ad_groups(client, customer_id, page_size, campaign_id):
    
    ga_service = client.get_service("GoogleAdsService")

    query = f"""
        SELECT campaign.id, ad_group.id, ad_group.name
        FROM ad_group
        WHERE
            ad_group.status = "ENABLED"
            and campaign.id = {campaign_id}
        """
    search_request = client.get_type("SearchGoogleAdsRequest")
    search_request.customer_id = customer_id
    search_request.query = query
    search_request.page_size = page_size
    results = ga_service.search(search_request)
    return results
