from google.ads.googleads.errors import GoogleAdsException

def get_campaigns(client, customer_id):
    ga_service = client.get_service("GoogleAdsService")

    query = """
        SELECT campaign.id, campaign.name
        FROM campaign
        WHERE campaign.status = "ENABLED"
        ORDER BY campaign.id"""

    # Issues a search request using streaming.
    search_request = client.get_type("SearchGoogleAdsStreamRequest")
    search_request.customer_id = customer_id
    search_request.query = query
    response = ga_service.search_stream(search_request)

    try:
        for batch in response:
            return batch.results
    except GoogleAdsException as ex:
        print("EXCEPTION IN GET_CAMPAIGN", ex.error.code().name)
        return -1

