This program creates campaigns in google_ads based on feed stored on BQ with all campaingns to create, defined by product team.

Every hour the program takes 50 campaings from the table configuration and create them using google_ads API.

The program needs credentials to access BQ and GCS on 'ms-guadiana' project, and credentials with access to google_ads. Them are stored as connections on airflow called: "gdn_storage_and_bq" and 
"gdn_campaign_creation_gads_credentials".